@extends('layouts.app')

<style>
    img{
        height: 100;
        width: 100;
    }

</style>

@section('content')

    <div>
        <img class="rounded mx-auto d-block" src="https://github.com/laravel/art/blob/master/laravel-logo.png?raw=true">
    </div>

    <div>
        <h2 class="pt-5 text-center">Featured Posts:</h2>
    </div>

    @if(count($posts) > 0)
       @foreach($posts as $post)
           <div class="card text-center">
               <div class="card-body">
                   <h4 class="card-title mb-3"><a href="/posts/{{$post->id}}">{{$post->title}}</a></h4>
                   <h6 class="card-text mb-3">Author: {{$post->user->name}}</h6>
                </div>
               {{-- if the authenticated user is the author of this blog post --}}
           </div>
       @endforeach
   @else
       <div>
           <h2>There are no posts to show</h2>
       </div>
   @endif
@endsection