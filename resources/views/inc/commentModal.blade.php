<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
  Comment
</button>

<!-- Modal -->
<form method="POST" action="/posts/{{$post->id}}/comment">
	@method('PUT')
	@csrf
	<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">

				<div class="mb-3 mx-2">
					<label for="message-text" class="col-form-label">Comment:</label>
					<textarea class="form-control" name="content" id="content"></textarea>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
					<button type="sumbit" class="btn btn-primary">Post comment</button>
				</div>

			</div>
		</div>
	</div>
</form>